package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Principal {

	private WebDriver driver;
	private String url = "http://localhost/SOH/index.php/";
	
	@BeforeTest
	public void precondiciones() {
		//Siempre_poner_esta_URL
		System.setProperty("webdriver.chrome.driver","C:\\Windows\\System32\\drivers\\chromedriver.exe");
		//abrir_el_navegador
		driver = new ChromeDriver();
		//Paso2_Ir_URL
		driver.get(url);
		
	}//fin_metodo
	
	@Test
	public void Test() {
		
//------------------------------------
//----TEST CASE 1:L istar:LOGUEARSE y Verificar que permita desplegar una lista de 
			//colaboradores que tiene horas ingresadas cuando este en la pantalla "Horas Colaborador".
		WebElement loginCedula;
		WebElement loginPass;
		WebElement inicioSesion;
		WebElement listaHorasColaborador;
		//WebElement eliminarHorasColaborador;
		WebElement opcionPersonal;
		String opcionPersonal_XPATH = "//*[@id=\"bs-sidebar-navbar-collapse-1\"]/ul[1]/li[2]";
		String listaHorasColaborador_XPATH = "//*[@id=\"bs-sidebar-navbar-collapse-1\"]/ul[1]/li[2]/ul/li[2]/a";
		
		//*********************************************
		//TEST CASE 2 Variables:
		WebElement opcionMas;
		String opcionMas_XPATH = "//*[@id=\"gcrud-search-form\"]/div[2]/table/tbody/tr[1]/td[2]/div[1]/div/button";
		WebElement opcionEliminar;
		String opcionEliminar_XPATH = "//*[@id=\"gcrud-search-form\"]/div[2]/table/tbody/tr[1]/td[2]/div[1]/div/ul/li[2]/a";
		WebElement opcionEliminar_Modal;
		String opcionEliminar_Modal_XPATH = "/html/body/div[1]/div[2]/div[2]/div/div/div[3]/button[2]";
		
		WebElement verModal;
		String opcionVermodal_XPATH = "/html/body/div[1]/div[2]/div[2]/div"; 
		

		
		// Obtenemos el WebElement correspondiente al textfield "Email Address"
			loginCedula = driver.findElement(By.name("cedula"));
			loginCedula.click();
			//Ingresar valores en el textfield "Cedula"
			loginCedula.sendKeys("123");
			

		//Recuperar el WebElement correspondiente a el Password Field
			loginPass = driver.findElement(By.name("clave"));
			loginPass.click();
			//Ingresar valores en el Password Field
			loginPass.sendKeys("root");
			esperarSegundos(driver, 2);
		
			inicioSesion= driver.findElement(By.cssSelector("body > div > div > div > div > div.panel-body > form > fieldset > input"));
			inicioSesion.click();
			
			//EntrarPestaņaPersonal
			opcionPersonal= driver.findElement(By.xpath(opcionPersonal_XPATH));
			opcionPersonal.click();
			esperarSegundos(driver, 2);
			
			//Entrar_Horas_colaboradores_LISTAR
			listaHorasColaborador = driver.findElement(By.xpath(listaHorasColaborador_XPATH));
			listaHorasColaborador.click();
			esperarSegundos(driver, 2);
//--------------------------------------------------------------------------------------------
//----TEST CASE 2:Verificar que se elimine las horas de un colaborador cuando se le de click a la opcion "Mas" y posteriormente "Eliminar"
			opcionMas = driver.findElement(By.xpath(opcionMas_XPATH));
			opcionMas.click();
			esperarSegundos(driver, 2);
	
			opcionEliminar = driver.findElement(By.xpath(opcionEliminar_XPATH));
			opcionEliminar.click();
			esperarSegundos(driver, 2);
			
			verModal = driver.findElement(By.xpath(opcionVermodal_XPATH));

			opcionEliminar_Modal = driver.findElement(By.xpath(opcionEliminar_Modal_XPATH));
			opcionEliminar_Modal.click();
			esperarSegundos(driver, 3);
			
	}//fin_metodo_TEST
	
	
	
	
	
	@AfterTest
	public void salir() {
		driver.quit();
	}//fin_metodo
	
	
	//Metodo para espera
	   public void esperarSegundos(WebDriver driver, int segundos){
		      synchronized(driver){
		         try {
		            driver.wait(segundos * 1000);
		         } catch (InterruptedException e) {
		            // TODO Auto-generated catch block
		            //e.printStackTrace();
		         }
		      }
		   }
		    
	
	
	
}//Fin class
