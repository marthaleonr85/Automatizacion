package main;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Secundario {


	private WebDriver driver;
	private String url = "http://localhost/SOH/index.php/";
	
	@BeforeTest
	public void precondiciones() {
		//Siempre_poner_esta_URL
		System.setProperty("webdriver.chrome.driver","C:\\Windows\\System32\\drivers\\chromedriver.exe");
		//abrir_el_navegador
		driver = new ChromeDriver();
		//Paso2_Ir_URL
		driver.get(url);
		
	}//fin_metodo
	
	@Test
	public void Test() {
		
//------------------------------------
//----TEST CASE 1:
		WebElement loginCedula;
		WebElement loginPass;
		WebElement inicioSesion;
		WebElement opcionSedes;
		String OpcionSedes_XPATH = "//*[@id=\"bs-sidebar-navbar-collapse-1\"]/ul[1]/li[5]/a";
		WebElement subMenuComites;
		String submenuComites_XPATH = "//*[@id=\"bs-sidebar-navbar-collapse-1\"]/ul[1]/li[5]/ul/li[1]/a";
		WebElement anadirComite;
		String anadirComite_XPATH = "//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a";
		WebElement opcionNombreComite;
		String nombreComite_XPATH = "//*[@id=\"nombreComite\"]";
		WebElement opcionTelefonoComite;
		String telefonoComite_XPATH = "//*[@id=\"telefono\"]";
		
		WebElement nombreSector;
		String nombreSector_XPATH = "//*[@id=\"field_codSector_chosen\"]";
		WebElement bajura;
		String bajura_XPATH = "//*[@id=\"field_codSector_chosen\"]/div/ul/li[2]";
		
		WebElement regionDelComite;
		String regionDelComite_XPATH = "//*[@id=\"field_codRegion_chosen\"]";
		WebElement regionChorotega;
		String regionChorotega_XPATH = "//*[@id=\"field_codRegion_chosen\"]/div/ul/li[2]";
		
		WebElement btnGuardarVolverLista;
		String btnGuardarVolverLista_XPATH = "//*[@id=\"save-and-go-back-button\"]";
		
		
		// Obtenemos el WebElement correspondiente al textfield "Email Address"
			loginCedula = driver.findElement(By.name("cedula"));
			loginCedula.click();
			//Ingresar valores en el textfield "Cedula"
			loginCedula.sendKeys("123");
			

		//Recuperar el WebElement correspondiente a el Password Field
			loginPass = driver.findElement(By.name("clave"));
			loginPass.click();
			//Ingresar valores en el Password Field
			loginPass.sendKeys("root");
			esperarSegundos(driver, 2);
		
			inicioSesion= driver.findElement(By.cssSelector("body > div > div > div > div > div.panel-body > form > fieldset > input"));
			inicioSesion.click();
			
			//Opcion Sedes
			opcionSedes = driver.findElement(By.xpath(OpcionSedes_XPATH));
			opcionSedes.click();
			esperarSegundos(driver, 2);
			
			//Opcion_SubMenuComites
			subMenuComites = driver.findElement(By.xpath(submenuComites_XPATH));
			subMenuComites.click();
			esperarSegundos(driver, 2);
			
			//Opcion_a�adir_un_Comite
			anadirComite = driver.findElement(By.xpath(anadirComite_XPATH));
			anadirComite.click();
			esperarSegundos(driver, 2);
			
			//Ingresar_Caracteres_COMITE
			opcionNombreComite = driver.findElement(By.xpath(nombreComite_XPATH));
			opcionNombreComite.click();
			opcionNombreComite.sendKeys("Nosara");
			esperarSegundos(driver, 2);

			//Ingresar_Caracteres_TELEFONO
			opcionTelefonoComite = driver.findElement(By.xpath(telefonoComite_XPATH));
			opcionTelefonoComite.click();
			opcionTelefonoComite.sendKeys("26873411");
			esperarSegundos(driver, 2);
			
			//Seleccionar_OPCION_Nombre_SECTOR
			nombreSector = driver.findElement(By.xpath(nombreSector_XPATH));
			nombreSector.click();
			esperarSegundos(driver, 2);
			
			//Seleccionar_SECTOR_Bajura
			bajura = driver.findElement(By.xpath(bajura_XPATH));
			bajura.click();
			esperarSegundos(driver, 2);
			
			
			//Seleccionar_OPCION_RegionDelComite
			regionDelComite = driver.findElement(By.xpath(regionDelComite_XPATH));
			regionDelComite.click();
			esperarSegundos(driver, 2);
			
			//Seleccionar_Region_Chorotega
			regionChorotega = driver.findElement(By.xpath(regionChorotega_XPATH));
			regionChorotega.click();
			esperarSegundos(driver, 2);
			
			//Guardar datos
			btnGuardarVolverLista = driver.findElement(By.xpath(btnGuardarVolverLista_XPATH));
			btnGuardarVolverLista.click();
			esperarSegundos(driver, 2);
			
			

			
	}//fin_metodo_TEST
	
	
	
	
	
	@AfterTest
	public void salir() {
		esperarSegundos(driver, 3);
		driver.quit();
	}//fin_metodo
	
	
	//Metodo para espera
	   public void esperarSegundos(WebDriver driver, int segundos){
		      synchronized(driver){
		         try {
		            driver.wait(segundos * 1000);
		         } catch (InterruptedException e) {
		            // TODO Auto-generated catch block
		            //e.printStackTrace();
		         }
		      }
		   }
		    
	
	
	
	
}//Fin Class
