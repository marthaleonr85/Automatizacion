package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Main {
	
	//variables
		//objeto_que_habla_con_el_navegador
		private WebDriver driver;
		//Declarar_URL
		private String url = "https://www.fifa.com/worldcup/";
	
		@BeforeTest
		public void precondiciones() {
			//Siempre_poner_esta_URL
			System.setProperty("webdriver.chrome.driver","C:\\Windows\\System32\\drivers\\chromedriver.exe");
			//abrir_el_navegador
			driver = new ChromeDriver();
			//Paso2_Ir_URL
			driver.get(url);
			
		}//fin_metodo
	
		@Test
		public void Test() {
			
			WebElement menuTeams;
			WebElement imagenCR;
			WebElement imagenSamara;
			WebElement titulo;
			
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String menuTeamsXpath = "html/body/div[2]/div/ul/li[2]/a";//absoluto
			String imagenCRCXPath = "//a[contains(@href, '43901')]";//relativo
			String imagenSamaraXPath = "//img[contains(@src, 'zwtsfdrmbhpopevtahin')]";
			String tituloXPath = "//h1[contains(@class, 'article')]";
			String tituloObtenido;
			String tituloEsperado = "Preview: Costa Rica v Serbia";
			
			
			//EncontaraElementoParaCargarlo
			menuTeams = driver.findElement(By.xpath(menuTeamsXpath));//absoluto
			menuTeams.click();//DarClickAlElemento
			
			
			imagenCR = driver.findElement(By.xpath(imagenCRCXPath));//relativo
			imagenCR.click();//DarClickAlElemento
			
			imagenSamara = driver.findElement(By.xpath(imagenSamaraXPath)); 
			imagenSamara.click();
			
			titulo = driver.findElement(By.xpath(tituloXPath));//encuentra elemeto
			tituloObtenido = titulo.getText();//para q obtenga el titulo
			
			
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(tituloEsperado, tituloObtenido);
			
		}//fin_metodo
		
		@AfterTest
		public void salir() {
			driver.quit();
		}//fin_metodo

}//Fin Class
